package com.company;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    // Staff class
    abstract static class Staff{
        protected int id;
        protected String name;
        protected String address;

        public void staffMember(int id, String name, String address){
            this.id = id;
            this.name = name;
            this.address = address;
        }
        @Override
        public String toString(){

            return ("ID : " + id +"\nName : "+ name +"\naddress : "+ address);
        }
        public abstract double pay ();

    }

    // Volunteer class
    static class Volunteer extends Staff {
        public void volunteer (int id, String name, String address){
            this.id = id;
            this.name = name;
            this.address = address;
        }
        @Override
        public String toString(){
            return ("ID : " + id +"\nName : "+ name +"\naddress : "+ address +"\nThanks you!"+"\n═══════════════════════════════════");
        }
        public double pay(){
            return 0;
        }

    }

    // SalarieEmployee
    static class SalarieEmployee extends Staff {

        private double salary;
        private double bonus;

        public void salarieEmployee (int id, String name, String address, double salary, double bonus){
            this.id = id;
            this.name = name;
            this.address = address;
            this.salary = salary;
            this.bonus = bonus;
        }
        @Override
        public String toString (){
            return ("ID : " + id +"\nName : "+ name +"\naddress : "+ address+"\nSalary : "+salary+"\nBonus : "+bonus +"\nPayment : "+ pay()+" $\n═══════════════════════════════════");
        }
        public double pay (){
            return salary + bonus;
        }

    }

    // HourlyEmployee
   static class HourlyEmployee extends Staff {
        private int hourWork;
        private double rate;
        public void hourlyEmployee (int id, String name, String address, int hourWork, double rate){
            this.id = id;
            this.name = name;
            this.address = address;
            this.hourWork = hourWork;
            this.rate = rate;
        }
        @Override
        public String toString (){
            return ("ID : " + id +"\nName : "+ name +"\naddress : "+ address+"\nHourWork : "+hourWork+"\nRate : "+rate +"\nPayment : "+ pay()+" $\n═══════════════════════════════════");
        }
        public double pay (){
            return hourWork * rate;
        }
    }

    //--------------------------------------------------
    // add Volunteer method
    public static void addVolunteer(Volunteer st){
        Scanner cin =new Scanner(System.in);
        System.out.println("---------- Add Volunteer-----------");
        do{
            System.out.print("ID : ");
            String text1 = cin.nextLine();
            Pattern pt1 = Pattern.compile("^[0-9]+$");
            Matcher mt1 = pt1.matcher(text1);
            if(mt1.find()){
                st.id = Integer.parseInt(text1);
                break;
            }else {
                System.out.println("Please input Number!");
            }
        }while (true);

        do{
            System.out.print("Name : ");
            String text2 = cin.nextLine();
            Pattern pt2 = Pattern.compile("^[a-z A-Z]+$");
            Matcher mt2 = pt2.matcher(text2);
            if(mt2.find()){
                st.name = text2;
                break;
            }else {
                System.out.println("Please input letter!");
            }
        }while (true);


        do{
            System.out.print("Address : ");
            String text3 = cin.nextLine();
            Pattern pt3 = Pattern.compile("^[a-z A-Z]+$");
            Matcher mt3 = pt3.matcher(text3);
            if(mt3.find()){
                st.address = text3;
                break;
            }else {
                System.out.println("Please input letter!");
            }
        }while (true);
    }

    // -----------------------------------------
    // Method add salarie Employee
    public static void addSalariEmployee(SalarieEmployee st){
        Scanner cin =new Scanner(System.in);
        System.out.println("---------- Add Volunteer-----------");
        do{
            System.out.print("ID : ");
            String text1 = cin.nextLine();
            Pattern pt1 = Pattern.compile("^[0-9]+$");
            Matcher mt1 = pt1.matcher(text1);
            if(mt1.find()){
                st.id = Integer.parseInt(text1);
                break;
            }else {
                System.out.println("Please input Number!");
            }
        }while (true);

        do{
            System.out.print("Name : ");
            String text2 = cin.nextLine();
            Pattern pt2 = Pattern.compile("^[a-z A-Z]+$");
            Matcher mt2 = pt2.matcher(text2);
            if(mt2.find()){
                st.name = text2;
                break;
            }else {
                System.out.println("Please input letter!");
            }
        }while (true);


        do{
            System.out.print("Address : ");
            String text3 = cin.nextLine();
            Pattern pt3 = Pattern.compile("^[a-z A-Z]+$");
            Matcher mt3 = pt3.matcher(text3);
            if(mt3.find()){
                st.address = text3;
                break;
            }else {
                System.out.println("Please input letter!");
            }
        }while (true);

        do{
            System.out.print("Salary : ");
            String text4 = cin.nextLine();
            Pattern pt4 = Pattern.compile("^[0-9.0-9]+$");
            Matcher mt4 = pt4.matcher(text4);
            if(mt4.find()){
                st.salary = Double.parseDouble(text4);
                break;
            }else {
                System.out.println("Please input Number!");
            }
        }while (true);

        do{
            System.out.print("Bonus : ");
            String text1 = cin.nextLine();
            Pattern pt1 = Pattern.compile("^[0-9.0-9]+$");
            Matcher mt1 = pt1.matcher(text1);
            if(mt1.find()){
                st.bonus = Double.parseDouble(text1);
                break;
            }else {
                System.out.println("Please input Number!");
            }
        }while (true);

    }


    // -----------------------------------------
    // Method add hourly Employee
    public static void addHourlyEmployee(HourlyEmployee st){
        Scanner cin =new Scanner(System.in);
        System.out.println("---------- Add Volunteer-----------");
        do{
            System.out.print("ID : ");
            String text1 = cin.nextLine();
            Pattern pt1 = Pattern.compile("^[0-9]+$");
            Matcher mt1 = pt1.matcher(text1);
            if(mt1.find()){
                st.id = Integer.parseInt(text1);
                break;
            }else {
                System.out.println("Please input Number!");
            }
        }while (true);

        do{
            System.out.print("Name : ");
            String text2 = cin.nextLine();
            Pattern pt2 = Pattern.compile("^[a-z A-Z]+$");
            Matcher mt2 = pt2.matcher(text2);
            if(mt2.find()){
                st.name = text2;
                break;
            }else {
                System.out.println("Please input letter!");
            }
        }while (true);


        do{
            System.out.print("Address : ");
            String text3 = cin.nextLine();
            Pattern pt3 = Pattern.compile("^[a-z A-Z]+$");
            Matcher mt3 = pt3.matcher(text3);
            if(mt3.find()){
                st.address = text3;
                break;
            }else {
                System.out.println("Please input letter!");
            }
        }while (true);

        do{
            System.out.print("Hour Work : ");
            String text4 = cin.nextLine();
            Pattern pt4 = Pattern.compile("^[0-9]+$");
            Matcher mt4 = pt4.matcher(text4);
            if(mt4.find()){
                st.hourWork = Integer.parseInt(text4);
                break;
            }else {
                System.out.println("Please input Number!");
            }
        }while (true);

        do{
            System.out.print("Rate : ");
            String text1 = cin.nextLine();
            Pattern pt1 = Pattern.compile("^[0-9.0-9]+$");
            Matcher mt1 = pt1.matcher(text1);
            if(mt1.find()){
                st.rate = Double.parseDouble(text1);
                break;
            }else {
                System.out.println("Please input Number!");
            }
        }while (true);

    }

    // -------------------------------
    //     Edit employee method
    //--------------------------------
    public static void edit(ArrayList<Staff> arr){
        Scanner cin = new Scanner(System.in);
        int id;
        do {
            System.out.println("=========== user info ==========");
            System.out.print("type ID to Edit : ");
            String text1 = cin.nextLine();
            Pattern pt1 = Pattern.compile("^[0-9]+$");
            Matcher mt1 = pt1.matcher(text1);
            if (mt1.find()) {
                id= Integer.parseInt(text1);
                for (var temp : arr) {
                    if(temp.id == id){
                        if (temp instanceof Volunteer) {
                            do{
                                System.out.print("Name : ");
                                String text2 = cin.nextLine();
                                Pattern pt2 = Pattern.compile("^[a-z A-Z]+$");
                                Matcher mt2 = pt2.matcher(text2);
                                if(mt2.find()){
                                    temp.name = text2;
                                    break;
                                }else {
                                    System.out.println("Please input letter!");
                                }
                            }while (true);


                            do{
                                System.out.print("Address : ");
                                String text3 = cin.nextLine();
                                Pattern pt3 = Pattern.compile("^[a-z A-Z]+$");
                                Matcher mt3 = pt3.matcher(text3);
                                if(mt3.find()){
                                    temp.address = text3;
                                    break;
                                }else {
                                    System.out.println("Please input letter!");
                                }
                            }while (true);
                        } else if (temp instanceof SalarieEmployee) {
                            do{
                                System.out.print("Name : ");
                                String text2 = cin.nextLine();
                                Pattern pt2 = Pattern.compile("^[a-z A-Z]+$");
                                Matcher mt2 = pt2.matcher(text2);
                                if(mt2.find()){
                                    temp.name = text2;
                                    break;
                                }else {
                                    System.out.println("Please input letter!");
                                }
                            }while (true);


                            do{
                                System.out.print("Address : ");
                                String text3 = cin.nextLine();
                                Pattern pt3 = Pattern.compile("^[a-z A-Z]+$");
                                Matcher mt3 = pt3.matcher(text3);
                                if(mt3.find()){
                                    temp.address = text3;
                                    break;
                                }else {
                                    System.out.println("Please input letter!");
                                }
                            }while (true);

                            do{
                                System.out.print("Salary : ");
                                String text4 = cin.nextLine();
                                Pattern pt4 = Pattern.compile("^[0-9.0-9]+$");
                                Matcher mt4 = pt4.matcher(text4);
                                if(mt4.find()){
                                    ((SalarieEmployee) temp).salary = Double.parseDouble(text4);
                                    break;
                                }else {
                                    System.out.println("Please input Number!");
                                }
                            }while (true);

                            do{
                                System.out.print("Bonus : ");
                                String text2 = cin.nextLine();
                                Pattern pt2 = Pattern.compile("^[0-9.0-9]+$");
                                Matcher mt2 = pt2.matcher(text2);
                                if(mt2.find()){
                                    ((SalarieEmployee) temp).bonus = Double.parseDouble(text2);
                                    break;
                                }else {
                                    System.out.println("Please input Number!");
                                }
                            }while (true);
                        } else {
                            do{
                                System.out.print("Name : ");
                                String text2 = cin.nextLine();
                                Pattern pt2 = Pattern.compile("^[a-z A-Z]+$");
                                Matcher mt2 = pt2.matcher(text2);
                                if(mt2.find()){
                                    temp.name = text2;
                                    break;
                                }else {
                                    System.out.println("Please input letter!");
                                }
                            }while (true);


                            do{
                                System.out.print("Address : ");
                                String text3 = cin.nextLine();
                                Pattern pt3 = Pattern.compile("^[a-z A-Z]+$");
                                Matcher mt3 = pt3.matcher(text3);
                                if(mt3.find()){
                                    temp.address = text3;
                                    break;
                                }else {
                                    System.out.println("Please input letter!");
                                }
                            }while (true);

                            do{
                                System.out.print("Hour Work : ");
                                String text4 = cin.nextLine();
                                Pattern pt4 = Pattern.compile("^[0-9]+$");
                                Matcher mt4 = pt4.matcher(text4);
                                if(mt4.find()){
                                    ((HourlyEmployee) temp).hourWork = Integer.parseInt(text4);
                                    break;
                                }else {
                                    System.out.println("Please input Number!");
                                }
                            }while (true);

                            do{
                                System.out.print("Rate : ");
                                String text6 = cin.nextLine();
                                Pattern pt6 = Pattern.compile("^[0-9.0-9]+$");
                                Matcher mt6 = pt6.matcher(text6);
                                if(mt6.find()){
                                    ((HourlyEmployee) temp).rate = Double.parseDouble(text6);
                                    break;
                                }else {
                                    System.out.println("Please input Number!");
                                }
                            }while (true);
                        }
                    }
                }
                return;
            }
        }while (true);
    }

    // -------------------------------
    //     Remove employee method
    //--------------------------------
    public static void removeEm (ArrayList<Staff> arr){
        Scanner cin = new Scanner(System.in);
        int id;
        do{
            System.out.println("=========== user info ==========");
            System.out.print("type ID to remove : ");
            String text1 = cin.nextLine();
            Pattern pt1 = Pattern.compile("^[0-9]+$");
            Matcher mt1 = pt1.matcher(text1);
            if(mt1.find()){
                id= Integer.parseInt(text1);

                for(var n:arr){
                    if(n.id==id){
                        arr.remove(n);
                        return;
                    }
                }
                System.out.println("You ID not found!");
                break;
            }else {
                System.out.println("Please input Number!");
            }
        }while (true);

    }
    // Main
    public static void main(String[] args) {
	// write your code here
        Scanner cin =new Scanner(System.in);
        int choose2, choose1;

        ArrayList<Staff> arr = new ArrayList<>();




        MENU:do{

            //Sort
            arr.sort(new Comparator<Staff>() {
                @Override
                public int compare(Staff o1, Staff o2) {
                    return o1.name.compareToIgnoreCase(o2.name);
                }
            });

            // Show
            System.out.println("═══════════════════════════════════");
            for(var temp:arr){
                System.out.println(temp.toString());
            }

            System.out.println("\n\n------- Staff Member --------");
            System.out.println("1. Add member");
            System.out.println("2. Edit");
            System.out.println("3. Remove");
            System.out.println("4. Exit");

            System.out.print("Choose Option (1-4): ");
            String text1 = cin.nextLine();
            Pattern pt1 = Pattern.compile("^[0-9]+$");
            Matcher mt1 = pt1.matcher(text1);

            if(mt1.find()){
                choose1 = Integer.parseInt(text1);
                switch (choose1){
                    case 1:
                        // Add Employee with different type
                        do{
                            System.out.println("\n======= Choose type Imployee =======");
                            System.out.println("1. Volunteer");
                            System.out.println("2. Salarie Employee");
                            System.out.println("3. Hourly Employe");
                            System.out.println("4. Back");

                            String text = cin.nextLine();
                            Pattern pt = Pattern.compile("^[0-9]+$");
                            Matcher mt = pt.matcher(text);

                            if(mt.find()){
                                choose2 = Integer.parseInt(text);

                                Volunteer voln = new Volunteer();
                                SalarieEmployee se = new SalarieEmployee();
                                HourlyEmployee he = new HourlyEmployee();

                                switch (choose2){
                                    case 1:
                                        addVolunteer(voln);
                                        arr.add(voln);
                                        break;
                                    case 2:
                                        addSalariEmployee(se);
                                        arr.add(se);
                                        break ;
                                    case 3:
                                        addHourlyEmployee(he);
                                        arr.add(he);
                                        break ;
                                    case 4:
                                        continue MENU;
                                }
                            }

                        }while (true);
                    case 2:
                        // Edit employee
                        if(arr.size()>0){
                            System.out.println("Sorry Teacher!\nI don't know how to know which object is Salarie, Volunteer, Hourly");
                        }else {
                            System.out.println("---------------------\nNo staff to edit!\n---------------------");
                        }
                        break ;
                    case 3:
                        // remove
                        if(arr.size()>0){
                        removeEm(arr);
                        }else {
                            System.out.println("---------------------\nNo staff to remove!\n---------------------");
                        }
                        break ;
                    case 4:
                        // Exit
                        System.out.println("-------------\n      Good bye!\n--------------");
                        return;
                }
            }

        }while (true);
    }
}
